import time
import os
from configs import BASE_DIR
from utils.mixins import BaseClassMixin
from colorama import Fore


class DataManagerMixin(BaseClassMixin):
    @classmethod
    def save_info(cls):
        os.chdir(BASE_DIR)
        print(Fore.LIGHTWHITE_EX + "Saving info ..")
        try:
            os.mkdir("data")
        except FileExistsError:
            pass

        os.chdir("data")
        with open("session.txt", "w"):
            pass

        cls.loder.save_session_to_file("session.txt")
        print(Fore.GREEN + "\tsaved session to\n")

    @classmethod
    def load_info(cls, username):
        print(Fore.LIGHTWHITE_EX + "Loading info ..\n")
        time.sleep(0.5)
        try:
            os.chdir(BASE_DIR + R"/data")
        except FileExistsError:
            pass

        cls.loder.load_session_from_file(username=username, filename="session.txt")
