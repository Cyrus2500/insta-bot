from utils.datastore import StoreClass
from colorama import Fore


class FollowerAnalyzerMixin:
    __slots__ = ["__site_data", "__load_and_store_type", "__loaded_data"]

    def __init__(self, site_data, load_and_store_type="file"):
        self.__site_data: dict = site_data
        self.__load_and_store_type = load_and_store_type
        self.__loaded_data: dict = self._read_data()

    def _read_data(self) -> dict:
        """
        Only use for read stored data.
        from:
            database or json file.
        :return: dict.
        """
        loader = StoreClass.set_loder(self.__load_and_store_type)
        read_data = loader.load()
        return read_data

    def _diff(self) -> bool:
        """
        Checking the difference between 'new data' and 'stored data'.

        :return: False if there is no difference between loaded data from 'database/jsonfile'
                and new data, else return True.

        """
        load_count = int(self.__loaded_data.get("followers_count", 0))
        site_count = int(self.__site_data.get("followers_count", 0))
        if site_count >= load_count:
            return False
        else:
            return True

    def search_unfollow(self) -> None:
        res = self._diff()
        if res:
            users = set(self.__loaded_data["followers_name"]) - set(
                self.__site_data["followers_name"]
            )
            print(
                Fore.RED
                + "\nThere is new followers that unfollowed you recently!... : "
                + Fore.LIGHTWHITE_EX
                + f"{users}"
            )

        else:
            print(Fore.GREEN + "\nNo one unfollowed you..")

    def search_none_follow(self):
        data = self.__site_data["not_following_back"]
        if len(data):
            print(Fore.LIGHTRED_EX + "some followings not back to you :")
            print(Fore.LIGHTWHITE_EX + f"\n\t{data}")
        else:
            print(Fore.LIGHTGREEN_EX + "All of followings follow back to you")
