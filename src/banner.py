import os
from colorama import Fore
import platform

_sys = platform.system()


def banner():
    bnr = R"""
  _____           _        ____        _   
 |_   _|         | |      |  _ \      | |  
   | |  _ __  ___| |_ __ _| |_) | ___ | |_ 
   | | | '_ \/ __| __/ _` |  _ < / _ \| __|
  _| |_| | | \__ \ || (_| | |_) | (_) | |_ 
 |_____|_| |_|___/\__\__,_|____/ \___/ \__|
                                           
"""
    if _sys == "Windows":
        os.system("cls")
    else:
        os.system("clear")

    print(Fore.BLUE + bnr)
