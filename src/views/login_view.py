import json
import os
import sys
import time
from utils.instagram import Instagram
from configs import BASE_DIR
from configs import register
from src import banner
from colorama import Fore


class LoginView:
    data = None

    def login(self):
        banner()
        time.sleep(0.5)
        result = register()

        if result:
            print(
                Fore.LIGHTWHITE_EX
                + "---- Enter your username and password for login ----\n\n",
            )
            time.sleep(0.5)
            print(
                Fore.LIGHTRED_EX
                + "\u2501["
                + Fore.GREEN
                + " InstaBot "
                + Fore.LIGHTRED_EX
                + "]\u2501"
                + Fore.LIGHTWHITE_EX
                + R" \\ "
                + Fore.YELLOW
                + " Login "
                + Fore.LIGHTRED_EX
                + "\u2501>"
            )
            time.sleep(0.5)
            try:
                username = input(Fore.LIGHTWHITE_EX + "\n$ Enter your -username- :")
                time.sleep(0.5)
                password = input(Fore.LIGHTWHITE_EX + "\n$ Enter your -password- :")
                banner()
                time.sleep(0.5)
                Instagram.base.login(username, password)

                if os.path.exists(BASE_DIR + r"/data/UserInfo.json"):
                    self.data = Instagram.initial(username).initial_site_data(
                        "file", store=False
                    )  # store=False
                else:
                    self.data = Instagram.initial(username).initial_site_data(
                        "file", store=True
                    )  # store=True
                    print("\n\tYou registered successfully in program.")
                Instagram.data_manager.save_info()

                return self.data
            except KeyboardInterrupt:
                sys.exit()

        else:
            try:
                with open(BASE_DIR + r"/data/UserInfo.json", "r") as file:
                    data = json.loads(file.read())
                Instagram.data_manager.load_info(data.get("user"))
                instance = Instagram.initial.get_user(data.get("user"))

                self.data = instance.initial_site_data(
                    "file", store=False
                )  # store=False

            except Exception:
                os.rmdir(BASE_DIR + R"/data")
                print(
                    Fore.LIGHTRED_EX + "***Error controlled!..check your connection***"
                )
                sys.exit()

        return self.data
