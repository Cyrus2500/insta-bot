import time
import sys
from src import banner
from colorama import Fore
from src.views import LoginView
from src.views import HomeView


class RootView:
    @staticmethod
    def root():
        banner()
        time.sleep(0.5)
        print(
            Fore.GREEN
            + "\t-[created  by %-Cyrus-% ] - instagram :_awmirsn_\n\n"
            + Fore.LIGHTCYAN_EX
            + "{1}   -Login-\n\n"
            + Fore.LIGHTWHITE_EX
            + "{00}   -Exit-\n\n"
        )
        try:
            chose = input(
                Fore.LIGHTRED_EX
                + "\u2501["
                + Fore.GREEN
                + " InstaBot "
                + Fore.LIGHTRED_EX
                + "]\u2501"
                + Fore.LIGHTWHITE_EX
                + R" \ "
                + Fore.YELLOW
                + " Root "
                + Fore.LIGHTWHITE_EX
                + R" \  "
                + Fore.LIGHTRED_EX
                + "\u2501 >"
                + Fore.LIGHTCYAN_EX
                + "\n$"
                + Fore.LIGHTWHITE_EX
                + " input : "
            )
        except KeyboardInterrupt:
            sys.exit()
        else:
            if chose == "1":
                data = LoginView().login()
                time.sleep(0.5)
                print(Fore.LIGHTYELLOW_EX + "\nredirecting to home view...")
                time.sleep(0.5)
                HomeView().home(data)
