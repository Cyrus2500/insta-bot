"""
Main Module

This module serves as the entry point for the program and initiates the execution of the desired functionality.

Usage:
    - Execute the main.py file to start the program.
"""

__version__ = "1.0.2"

from src.views import RootView

if __name__ == "__main__":
    RootView.root()
