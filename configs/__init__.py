from configs.path import BASE_DIR
from configs.reg import register

__all__ = ["BASE_DIR", "register"]
